package com.example.loginregistrarse

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth

class registrarse : AppCompatActivity() {
    private lateinit var firebaseAuth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_registrarse)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val emailRegistro : TextView = findViewById(R.id.emailRegistro2)
        val contrasenaRegistro : TextView = findViewById(R.id.contrasenaRegistro)
        val botonRegistroNuevo : TextView = findViewById(R.id.botonRegistroNuevo)
        val botonloguearseregistro : TextView = findViewById(R.id.BotonLoguearseRegistro)
        botonRegistroNuevo.setOnClickListener(){
            crearCuenta(emailRegistro.text.toString(),contrasenaRegistro.text.toString())
        }
        firebaseAuth= Firebase.auth
        botonloguearseregistro.setOnClickListener(){
            val i = Intent (this,MainActivity::class.java)
            startActivity(i)
        }
    }
    private fun crearCuenta(email: String, password:String){
        firebaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this){task ->
            if(task.isSuccessful){
                Toast.makeText(baseContext,"Cuenta creada",Toast.LENGTH_SHORT).show()
                val i = Intent(this, MainActivity2::class.java)
                startActivity(i)
            }else{
                Toast.makeText(baseContext,"Error cuenta ya existente",Toast.LENGTH_SHORT).show()
            }
        }
    }
}