package com.example.loginregistrarse

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth

class MainActivity : AppCompatActivity() {
    private lateinit var firebaseAuth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val botonLogin : Button = findViewById(R.id.botonRegistro)
        val email : TextView = findViewById(R.id.email)
        val contrasena : TextView = findViewById(R.id.contrasena)
        val botonRegistroLogin : TextView = findViewById(R.id.BotonRegistroLogin)

        firebaseAuth = Firebase.auth
        botonLogin.setOnClickListener(){
            signIn(email.text.toString(),contrasena.text.toString())
        }
        botonRegistroLogin.setOnClickListener(){
            val i = Intent (this,registrarse::class.java)
            startActivity(i)
        }
    }
    private fun signIn(email:String, password:String){
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this){ task ->
            if (task.isSuccessful) {
                //aqui camos a ir a la pagina despues de iniciar sesion o loguearse
                Toast.makeText(baseContext,"Inicio de sesion correcto",Toast.LENGTH_SHORT).show()
                val i = Intent(this, MainActivity2::class.java)
                startActivity(i)
            }else{
                Toast.makeText(baseContext,"error de email o contraseña", Toast.LENGTH_SHORT).show()
            }
        }
    }
}